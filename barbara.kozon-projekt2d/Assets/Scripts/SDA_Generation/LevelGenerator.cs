using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Generation
{
    public class LevelGenerator : MonoBehaviour
    {
        [Header("Shield")]
        [SerializeField]
        private Transform shieldPos;
        [SerializeField]
        private Transform shieldRoot;
        
        [SerializeField]
        private BaseShield[] simpleShields;
        [SerializeField]
        private BaseShield[] bossShields;

        [Header("Arrow")]
        [SerializeField]
        private Transform arrowPos;
        [SerializeField]
        private Transform arrowRoot;
        [SerializeField]
        private Arrow arrowPrefab;


        public BaseShield SpawnShield(StageType stageType)
        {
            BaseShield shieldToSpawn = default;
            //var shieldToSpawn = default(BaseShield);

            if (stageType == StageType.Normal)
            {
                var randomIndex = Random.Range(0, simpleShields.Length);
                shieldToSpawn = simpleShields[randomIndex];
            }
            else
            {
                var randomIndex = Random.Range(0, bossShields.Length);
                shieldToSpawn = bossShields[randomIndex];
            }

            var shieldObj = Instantiate(shieldToSpawn,
                shieldPos.position, shieldPos.rotation);

            shieldObj.transform.SetParent(shieldRoot);

            return shieldObj;
        }


        public Arrow SpawnArrows()
        {
            var arrowObj = Instantiate(arrowPrefab, 
                arrowPos.position, arrowPos.rotation);
            arrowObj.transform.SetParent(arrowRoot);

            return arrowObj;
        }
    } 
}
