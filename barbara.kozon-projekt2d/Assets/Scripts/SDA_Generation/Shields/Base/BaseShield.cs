using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using SDA.Score;
using SDA.UI;
using SDA.Architecture;

namespace SDA.Generation
{
    public abstract class BaseShield : MonoBehaviour
    {
        private UnityAction onShieldHit;
        private UnityAction onWin;

        [SerializeField]
        private int arrowsToWin;
        public int ArrowsToWin => arrowsToWin;

        [SerializeField]
        protected ShieldMovementStep[] movementScheme;

        [SerializeField]
        private List<Arrow> arrowsInShield = new List<Arrow>();

        
        public virtual void Initialize(UnityAction onShieldHit, UnityAction onWinCallback)
        {
            this.onShieldHit = onShieldHit;
            onWin = onWinCallback;
        }
        public abstract void Rotate();
        public virtual void Dispose()
        {
            for (int i = arrowsInShield.Count - 1; i >= 0; i--)
            {
                var arrow = arrowsInShield[i];
                Destroy(arrow.gameObject);
                arrowsInShield.Remove(arrow);
            }
            arrowsInShield.Clear();
            onShieldHit = null;
            onWin = null;

            Destroy(this.gameObject);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            var arrow = other.GetComponentInParent<Arrow>();
            arrow.Rigidbody2D.velocity = Vector2.zero;
            arrow.transform.rotation = Quaternion.identity;
            arrow.Rigidbody2D.isKinematic = true;
            arrow.transform.position = new Vector3(0f, 0.24f, 0f);
            arrowsInShield.Add(arrow);
            arrow.Deinit();
            arrow.transform.SetParent(this.transform);
            
            onShieldHit.Invoke();
            
            if (arrowsInShield.Count == arrowsToWin)
            {
                onWin.Invoke();
            }
            
        }

    }
}
