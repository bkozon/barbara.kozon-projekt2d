using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Generation
{
    public class Arrow : MonoBehaviour
    {
        private UnityAction onLose;

        [SerializeField]
        private Rigidbody2D rigidbody2D;
        [SerializeField]
        private float speed;
        public Rigidbody2D Rigidbody2D => rigidbody2D;

        public void InitArrow(UnityAction onLoseCallback)
        {
            this.onLose = onLoseCallback;
        }

        public void Deinit()
        {
            this.onLose = null;
        }

        public void ThrowArrow()
        {
            rigidbody2D.AddForce(Vector2.up * (speed*Time.fixedDeltaTime), ForceMode2D.Impulse);
        }

        public void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Arrow"))
            {
                onLose?.Invoke();
                onLose = null;
            }
        }

        public void DestroyArrow()
        {
            Destroy(this.gameObject);
        }
    }
}
