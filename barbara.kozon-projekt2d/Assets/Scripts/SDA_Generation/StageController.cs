using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Generation
{
    public enum StageType
    {
        Normal,
        Boss,
    }
    public class StageController
    {
        private int bossPeriod= 5;
        private int currentStage;
        public int CurrentStageModulo => currentStage % bossPeriod;
        public int CurrentStage => currentStage;

        public void InitController()
        {
            currentStage = 0;
        }

        public StageType NextStage()
        {
            currentStage++;
            if (currentStage%bossPeriod==0)
            {
                return StageType.Boss;
            }
            return StageType.Normal;
        }
    }
}
