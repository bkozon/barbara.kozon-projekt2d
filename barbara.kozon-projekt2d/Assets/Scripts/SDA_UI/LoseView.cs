using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SDA.UI
{
    public class LoseView : BaseView
    {
        [SerializeField]
        private Button restartButton;
        public Button RestartButton => restartButton;

        [SerializeField]
        private Button backToMenuButton;
        public Button BackToMenuButton => backToMenuButton;

        [SerializeField]
        private Button settingsButton;

        public Button SettingsButton => settingsButton;

        [SerializeField]
        public TextMeshProUGUI scoreInfoText;

        [SerializeField]
        public TextMeshProUGUI stageInfoText;

        public void UpdatePointsAndStage(int points,int stage)
        {
            stageInfoText.text = $"STAGE {stage}";
            scoreInfoText.text = points.ToString();
        }

    } 
}
