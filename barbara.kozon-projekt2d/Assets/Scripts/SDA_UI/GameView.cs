using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace SDA.UI
{
    public class GameView : BaseView
    {
        [SerializeField]
        public TextMeshProUGUI scoreText;

        [SerializeField]
        private ArrowElement arrowElementPrefab;

        [SerializeField]
        private RectTransform arrowElementContent;

        [SerializeField]
        private DotElement[] elements;

        [SerializeField]
        private TextMeshProUGUI stageInfo;

        private List<ArrowElement> spawnedElements = new List<ArrowElement>();

        private int arrowToDelete;

        public void UpdateScore(int points)
        {
            scoreText.text = points.ToString();
        }

        public void SpawnAmmo(int amount)
        {
            DespawnAmmo();

            for (int i = 0; i < amount; ++i)
            {
                var newArrow = Instantiate(arrowElementPrefab, arrowElementContent);
                spawnedElements.Add(newArrow);
                newArrow.MarkAsUnlocked();

            }
            arrowToDelete = -1;

        }
        private void DespawnAmmo()
        {
            for (int i = spawnedElements.Count - 1; i >= 0; i--)
            {
                Destroy(spawnedElements[i].gameObject);
            }
            spawnedElements.Clear();
        }
        public void DecreaseAmmo()
        {
            arrowToDelete++;
            spawnedElements[arrowToDelete].MarkAsLocked();
        }
        public void UpdateStage(int currentStage)
        {
            if (currentStage == 0)
            {
                stageInfo.text = $"BOSS FIGHT";
                for (int i = 0; i < elements.Length; ++i)
                {
                    elements[i].gameObject.SetActive(false);
                }
                elements[elements.Length - 1].gameObject.SetActive(true);
            }
            else
            {
                if (currentStage == 1)
                {
                    for (int i = 0; i < elements.Length; ++i)
                    {
                        elements[i].gameObject.SetActive(true);
                        elements[i].MarkAsLocked();
                    }
                    elements[elements.Length - 1].gameObject.SetActive(false);

                }

                elements[currentStage - 1].MarkAsUnlocked();
                stageInfo.color = Color.white;
                stageInfo.text = $"STAGE {currentStage}";
            }


        }


    }
}
