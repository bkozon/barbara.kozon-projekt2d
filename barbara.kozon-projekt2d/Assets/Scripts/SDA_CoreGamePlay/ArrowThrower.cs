using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDA.Generation;

namespace SDA.CoreGamePlay
{
    public class ArrowThrower
    {
        private Arrow arrowToThrow;

        public void SetArrow(Arrow newArrow)
        {
            this.arrowToThrow = newArrow;
        }

        public void Throw()
        {
            if (arrowToThrow != null)
            {
                arrowToThrow.ThrowArrow();
                arrowToThrow = null;
            }
        }
    }
}
