using SDA.Generation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.CoreGamePlay
{
    public class ShieldMovementController
    {
        private BaseShield currentlyActiveShield;
        UnityAction onShieldHitCallback;
        UnityAction onWinCallback;

        public void InitializeShield(BaseShield newShield, UnityAction onShieldHit, UnityAction onWinCallback)
        {
            //destroy old shield
            if (currentlyActiveShield != null)
            {
                currentlyActiveShield.Dispose();
            }
            currentlyActiveShield = newShield;
            currentlyActiveShield.Initialize(onShieldHit,onWinCallback);
        }

        public void DisposeShield()
        {
            if (currentlyActiveShield != null)
            {
                currentlyActiveShield.Dispose();
            }
        }

        public void UpdateController()
        {
            if (currentlyActiveShield != null)
                currentlyActiveShield.Rotate();
        }

    }
}
