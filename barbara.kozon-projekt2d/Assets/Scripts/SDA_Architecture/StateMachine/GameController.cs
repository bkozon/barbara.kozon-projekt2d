using SDA.CoreGamePlay;
using SDA.Data;
using SDA.Generation;
using SDA.Input;
using SDA.Score;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Architecture
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private MenuView menuView;
        [SerializeField]
        private GameView gameView;
        [SerializeField]
        private SettingsView settingsView;
        [SerializeField]
        private LoseView loseView;
       
        [SerializeField]
        private LevelGenerator levelGenerator;

        private InputSystem inputSystem;
        private ShieldMovementController shieldMovementController;
        private ArrowThrower arrowThrower;
        private StageController stageController;
        private ScoreSystem scoreSystem;
        private SaveSystem saveSystem;
        
        private MenuState menuState;
        private GameState gameState;
        private SettingsState settingsState;
        private LoseState loseState;
        
        private BaseState currentlyActiveState;

        private UnityAction toGameStateTransition;
        private UnityAction toSettingsStateTransition;
        private UnityAction toLoseStateTransition;
        private UnityAction toMenuStateTransition;

        
       
        private void Start()
        {
            toGameStateTransition = () => ChangeState(gameState);
            toSettingsStateTransition = () => ChangeState(settingsState);
            toLoseStateTransition = () => ChangeState(loseState);
            toMenuStateTransition = () => ChangeState(menuState);
                        
            inputSystem = new InputSystem();
            stageController = new StageController();
            
            shieldMovementController = new ShieldMovementController();
            arrowThrower = new ArrowThrower();
            scoreSystem = new ScoreSystem();
            saveSystem = new SaveSystem();
           
            menuState = new MenuState(menuView,toGameStateTransition,toSettingsStateTransition,saveSystem);
            gameState = new GameState(gameView, inputSystem, levelGenerator, shieldMovementController, arrowThrower, stageController,scoreSystem,toLoseStateTransition);
            settingsState = new SettingsState(settingsView);
            loseState = new LoseState(loseView,toMenuStateTransition,toGameStateTransition,scoreSystem,stageController);

            saveSystem.LoadGame();
            ChangeState(menuState);
        }

        private void Update()
        {
            currentlyActiveState?.UpdateState();
        }

        private void OnDestroy()
        {
            
        }

        private void ChangeState(BaseState newState)
        {
            currentlyActiveState?.DestroyState();
            currentlyActiveState = newState;
            currentlyActiveState?.InitState();
        }
    } 
}
