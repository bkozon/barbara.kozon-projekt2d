using SDA.Data;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Architecture
{
    public class MenuState : BaseState

    {
        private MenuView menuView;

        private UnityAction transitionToGameState;
        private UnityAction transitionToSettingsState;

        private SaveSystem saveSystem;


        public MenuState(MenuView menuView, UnityAction transitionToGameState,
            UnityAction transitionToSettingsState, SaveSystem saveSystem)
        {
            this.menuView = menuView;
            this.transitionToGameState = transitionToGameState;
            this.transitionToSettingsState = transitionToSettingsState;
            this.saveSystem = saveSystem;

        }
        public override void InitState()
        {
            Debug.Log("INIT MENU");
            if (menuView != null)
            {
                menuView.ShowView();
            }

            menuView.PlayButton.onClick.AddListener(transitionToGameState);
            menuView.SettingsButton.onClick.AddListener(transitionToSettingsState);

        }
        public override void UpdateState()
        {

        }
        public override void DestroyState()
        {
            if (menuView != null)
            {
                menuView.HideView();
            }
            menuView.PlayButton.onClick.RemoveAllListeners();
            menuView.SettingsButton.onClick.RemoveAllListeners();
        }
    }
}