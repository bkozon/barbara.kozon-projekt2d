using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Architecture
{
    public class SettingsState : BaseState
    {
        private SettingsView settingsView;

        public SettingsState(SettingsView settingsView)
        {
            this.settingsView = settingsView;
        }
        public override void InitState()
        {
            if (settingsView!=null)
            {
                settingsView.ShowView();
            }
        }

        public override void UpdateState()
        {
            
        }

        public override void DestroyState()
        {
            if (settingsView!=null)
            {
                settingsView.HideView();
            }
        }
    } 
}
