using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDA.UI;
using SDA.Input;
using SDA.Generation;
using SDA.CoreGamePlay;
using SDA.Score;
using UnityEngine.Events;
using DG.Tweening;

namespace SDA.Architecture
{
    public class GameState : BaseState
    {
        private GameView gameView;
        private InputSystem inputSystem;
        private LevelGenerator levelGenerator;
        private ShieldMovementController shieldMovementController;
        private ArrowThrower arrowThrower;
        private StageController stageController;
        private ScoreSystem scoreSystem;

        private UnityAction toLoseStateTransition;
        
        
        public GameState(GameView gameView, InputSystem inputSystem, LevelGenerator levelGenerator, 
            ShieldMovementController shieldMovementController, ArrowThrower arrowThrower, StageController stageController, ScoreSystem scoreSystem, UnityAction toLoseStateTransition)
        {
            this.gameView = gameView;
            this.inputSystem = inputSystem;
            this.levelGenerator = levelGenerator;
            this.shieldMovementController = shieldMovementController;
            this.arrowThrower = arrowThrower;
            this.stageController = stageController;
            this.scoreSystem = scoreSystem;
            this.toLoseStateTransition = toLoseStateTransition;
            
           

        }
        public override void InitState()
        {
            if (gameView!=null)
            {
                gameView.ShowView();
            }
            
            scoreSystem.InitSystem();
            stageController.InitController();
            PrepareNewShield();
            PrepareNewArrow();
            inputSystem.AddListener(arrowThrower.Throw);
                   
        }

        public override void UpdateState()
        {
            
            inputSystem.UpdateSystem();
            shieldMovementController.UpdateController();
        }
        public override void DestroyState()
        {
            if (gameView != null)
            {
                gameView.HideView();
            }
            shieldMovementController.DisposeShield();
            inputSystem.RemoveAllListeners();
        }

        private void PrepareNewArrow()
        {
            IncreasePoints();
            var newArrow = levelGenerator.SpawnArrows();
            newArrow.InitArrow(() => LoseGame(newArrow));
            arrowThrower.SetArrow(newArrow);
        }
        private void IncreasePoints()
        {
            scoreSystem.IncreasePoints();
            gameView.UpdateScore(scoreSystem.CurrentPoints);
        }

        private void PrepareNewShield()
        {
            var nextStageType = stageController.NextStage();
            var newShield = levelGenerator.SpawnShield(nextStageType);

            UnityAction onShieldHit = gameView.DecreaseAmmo;
            onShieldHit += PrepareNewArrow;

            shieldMovementController.InitializeShield(newShield, onShieldHit, PrepareNewShield);

            gameView.SpawnAmmo(newShield.ArrowsToWin);
            gameView.UpdateStage(stageController.CurrentStageModulo); 
        }

        private void LoseGame(Arrow lastArrow)
        {
            inputSystem.RemoveAllListeners();

            lastArrow.Rigidbody2D.gravityScale = 1f;
            lastArrow.Rigidbody2D.freezeRotation = false;
            lastArrow.Rigidbody2D.velocity = Vector2.zero;
            lastArrow.Rigidbody2D.AddTorque(18f, ForceMode2D.Impulse);

            var loseSequence = DOTween.Sequence();
            loseSequence
                .SetDelay(1f)
                .OnComplete(() => DestroyArrowAndProceed(lastArrow));
        }

        private void DestroyArrowAndProceed(Arrow lastArrow)
        {
            lastArrow.DestroyArrow();
            toLoseStateTransition.Invoke();
        }
    }
}
