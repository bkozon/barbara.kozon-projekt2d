using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Data
{
    [Serializable]
    public class PlayerData
    {
        public int stage;
        public int score;
        public int currency;

        public PlayerData(int stage, int score,int currency)
        {
            this.stage = stage;
            this.score = score;
            this.currency = currency;

        }
    }
    public class SaveSystem
    {
        private PlayerData data;
        public PlayerData Data => data;

        public void LoadGame()
        {
            if (PlayerPrefs.HasKey("PLAYER_SAVE"))
            {
                data = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PLAYER_SAVE"));
            }
            else
            {
                data = new PlayerData(0, 0, 200);
                SaveGame();
            }
        }

        public void SaveGame()
        {
            PlayerPrefs.SetString("PLAYER_SAVE", JsonUtility.ToJson(data));
        }


    }
}
